<section id="myCarousel" class="carousel slide">
    <div class="row" align="center" id="slider">

        <div class="col-xs-12">
            <div class="carousel-inner">
                <div class="item active">
                    <!-- Set the first background image using inline CSS below. -->
                    <div><img src="./assets/images/1680.jpg"></div>
                    <!--            <div class="fill" style="background-image:url('assets/images/1680.jpg');"></div>-->
                </div>
                <div class="item">
                    <!-- Set the second background image using inline CSS below. -->
                    <div><img src="./assets/images/1680.jpg"></div>
                </div>
                <div class="item">
                    <!-- Set the third background image using inline CSS below. -->
                    <div><img src="./assets/images/1680.jpg"></div>
                </div>
                <div class="item">
                    <!-- Set the third background image using inline CSS below. -->
                    <div><img src="./assets/images/1680.jpg"></div>
                </div>
                <div class="item">
                    <!-- Set the third background image using inline CSS below. -->
                    <div><img src="./assets/images/1680.jpg"></div>
                </div>
                <div class="insideslide">
                    <h1 style="color:#02baab">Creating a unique look</h1>

                    <h1 style="color:white">Never been easier</h1>
                    <button class="btn seefeatures">SEE FEATURES</button>
                    <button class="btn purchasenow">PURCHASE NOW</button>
                    <!-- Indicators -->
                    <ol class="carousel-indicators" id="">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                        <li data-target="#myCarousel" data-slide-to="4"></li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- Wrapper for Slides -->


        <!-- Controls -->
        <!-- <a class="left carousel-control" href="#myCarousel" data-slide="prev">
             <span class="icon-prev"></span>
         </a>
         <a class="right carousel-control" href="#myCarousel" data-slide="next">
             <span class="icon-next"></span>
         </a>-->


    </div>

</section>
<section id="stealofthemonth">
    <div class="row" align="center" id="sotm">
        <div class="col-xs-3"></div>
        <div class="col-xs-6">
            <h1>Steal of the month</h1>
            <hr>
            <p>That we can tuck in our children at night and know that they are fed and clothed and safe from harm. Our
                trails and triumphs become at once unique and universal</p>

            <button class="btn sotmbtn">PURCHASE NOW</button>
        </div>
        <div class="col-xs-3"></div>
    </div>
</section>
<section id="lw">
    <div class="row" align="center">
        <div class="col-xs-3"></div>
        <div class="col-xs-6">
            <h1>Latest works</h1>
            <hr>
            <p>That we can tuck in our children at night and know that they are fed and clothed and safe from harm. Our
                trails and triumphs become at once unique and universal</p>
        </div>
        <div class="col-xs-3"></div>
    </div>
    <div class="row" style="margin-top:4%;padding-bottom: 8%; " align="center">
        <div class="col-xs-3"></div>
        <div class="col-xs-2 box" id="latestbox1">
            <div class="boxbuttons boxbuttons1">
                <li>
                    <button class="btn"><span class="glyphicon glyphicon-zoom-in"></span></button>
                </li>
                <li>
                    <button class="btn"><span class="glyphicon glyphicon-heart"></span></button>
                </li>
                <li>
                    <button class="btn"><span class="glyphicon glyphicon-link"></span></button>
                </li>
            </div>
            <div class="lwboxbottom lwboxbottom1" align="left">
                <p id="lwboxfirsttext"> Linemans Wine</p>

                <p id="lwboxsecondtext"> Art Direction, Web Design</p>
            </div>
        </div>

        <div class="col-xs-2 box" id="latestbox2">
            <div class="boxbuttons boxbuttons2">
                <li>
                    <button class="btn"><span class="glyphicon glyphicon-zoom-in"></span></button>
                </li>
                <li>
                    <button class="btn"><span class="glyphicon glyphicon-heart"></span></button>
                </li>
                <li>
                    <button class="btn"><span class="glyphicon glyphicon-link"></span></button>
                </li>
            </div>
            <div class="lwboxbottom lwboxbottom2" align="left">
                <p id="lwboxfirsttext"> Linemans Wine</p>

                <p id="lwboxsecondtext"> Art Direction, Web Design</p>
            </div>
        </div>

        <div class="col-xs-2 box" id="latestbox3">
            <div class="boxbuttons boxbuttons3">
                <li>
                    <button class="btn"><span class="glyphicon glyphicon-zoom-in"></span></button>
                </li>

                <li>
                    <button class="btn"><span class="glyphicon glyphicon-heart"></span></button>
                </li>

                <li>
                    <button class="btn"><span class="glyphicon glyphicon-link"></span></button>
                </li>
            </div>
            <div class="lwboxbottom lwboxbottom3" align="left" >
                <p id="lwboxfirsttext">Marketing Materials & Branding</p>

                <p id="lwboxsecondtext"> Photography, Web Design</p>
            </div>
        </div>
        <div class="col-xs-3"></div>
    </div>
</section>
<section id="superbfeatures">
    <div class="row" id="sf" align="center">
        <div class="col-xs-2"></div>
        <div class="col-xs-8">
            <h1>Superb Features</h1>
            <hr>
            <div class="row">
                <div class="col-xs-1"></div>
                <div class="col-xs-1" id="icons"><span class="glyphicon glyphicon-star"></div>
                <div class="col-xs-4" align="left">
                    <h3>Brilliant Creative Design</h3>

                    <p>Ah well! it means much the same thing, said the Duchess digging her sharp
                        little chin into alice shoulder as she added, and the mora,of that m-dash</p>
                </div>
                <div class="col-xs-1" id="icons"><span class="glyphicon glyphicon-glass"></div>
                <div class="col-xs-4" align="left">
                    <h3>Brilliant Creative Design</h3>

                    <p>Ah well! it means much the same thing, said the Duchess digging her sharp
                        little chin into alice shoulder as she added, and the mora,of that m-dash</p>
                </div>
                <div class="col-xs-1"></div>
            </div>
            <div class="row" style="margin-top:60px;">
                <div class="col-xs-1"></div>
                <div class="col-xs-1" id="icons"><span class="glyphicon glyphicon-star"></div>
                <div class="col-xs-4" align="left">
                    <h3>Brilliant Creative Design</h3>

                    <p>Ah well! it means much the same thing, said the Duchess digging her sharp
                        little chin into alice shoulder as she added, and the mora,of that m-dash</p>
                </div>
                <div class="col-xs-1" id="icons"><span class="glyphicon glyphicon-cloud-download"></div>
                <div class="col-xs-4" align="left">
                    <h3>Brilliant Creative Design</h3>

                    <p>Ah well! it means much the same thing, said the Duchess digging her sharp
                        little chin into alice shoulder as she added, and the mora,of that m-dash</p>
                </div>
                <div class="col-xs-1"></div>
            </div>
        </div>
        <div class="com-xs-2"></div>

    </div>

</section>
<section id="secondslider" class="carousel slide">
    <div class="row" align="center" id="slider2">
        <div class="col-xs-12">
            <div class="carousel-inner">
                <div class="item active">
                    <!-- Set the first background image using inline CSS below. -->
                    <div><img src="./assets/images/slider2.jpg"></div>
                    <!--            <div class="fill" style="background-image:url('assets/images/1680.jpg');"></div>-->
                </div>
                <div class="item">
                    <!-- Set the second background image using inline CSS below. -->
                    <div><img src="./assets/images/slider2.jpg"></div>
                </div>
                <div class="item">
                    <!-- Set the third background image using inline CSS below. -->
                    <div><img src="./assets/images/slider2.jpg"></div>
                </div>
                <div class="item">
                    <!-- Set the third background image using inline CSS below. -->
                    <div><img src="./assets/images/slider2.jpg"></div>
                </div>
                <div class="item">
                    <!-- Set the third background image using inline CSS below. -->
                    <div><img src="./assets/images/slider2.jpg"></div>
                </div>
                <div class="bullets">
                    <!-- Indicators -->
                    <ol class="carousel-indicators" id="">
                        <li data-target="#secondslider" data-slide-to="0" class="active"></li>
                        <li data-target="#secondslider" data-slide-to="1"></li>
                        <li data-target="#secondslider" data-slide-to="2"></li>
                        <li data-target="#secondslider" data-slide-to="3"></li>
                        <li data-target="#secondslider" data-slide-to="4"></li>
                    </ol>
                </div>
            </div>
        </div>
</section>
<section id="latestblogpost">
    <div class="row" id="lbp" align="center">
        <div class="col-xs-2"></div>
        <div class="col-xs-8"><h1>Latest Blog Post</h1></div>
        <div class="col-xs-2"></div>
    </div>
    <div class="row" style="margin-top:4%;padding-bottom:8%;">
        <div class="col-xs-2"></div>
        <div class="col-xs-3 blogblocks">
            <img src="assets/images/blogimage.jpg" >
            <h2>Runway to Red Carpet:</br>Award Season</h2>

            <p> Her father worked on oil rigs and farms that through most of the depression. But this is a new story that has seared into my genetic</p>
            <a>learnmore</a>
            <button class="btn" style="float:right;"><span class="glyphicon glyphicon-cloud-download">465</button>
        </div>
        <div class="col-xs-3 blogblocks">
            <img src="assets/images/blogimage.jpg" >
            <h2>Runway to Red Carpet:</br>Award Season</h2>

            <p> Her father worked on oil rigs and farms that through most of the depression. But this is a new story that has seared into my genetic</p>
            <a>learnmore</a>
            <button class="btn" style="float:right;"><span class="glyphicon glyphicon-cloud-download">465</button>
        </div>
        <div class="col-xs-3 blogblocks">
            <img src="assets/images/blogimage.jpg" >
            <h2>Runway to Red Carpet:</br>Award Season</h2>

            <p> Her father worked on oil rigs and farms that through most of the depression. But this is a new story that has seared into my genetic</p>
            <a>learnmore</a>
            <button class="btn" style="float:right;"><span class="glyphicon glyphicon-cloud-download">465</button>
        </div>
        <div class="col-xs-1"></div>
    </div>

</section>
<section id="companyexperience">
    <div class="row" id="ce" align="center">
        <div class="col-xs-3"></div>
        <div class="col-xs-6">
            <h1>Company Experience</h1>
            <hr>
        </div>
        <div class="col-xs-3"></div>
    </div>
</section>
<script type="text/javascript">
    $('.carousel').carousel({
        interval: 5000 //changes the speed

    });

    $("#latestbox1").mouseenter(function () {
        $('.lwboxbottom1').hide();
        $('.boxbuttons1').css('visibility', 'visible');
        $('#latestbox1').addClass('hide2');
    });
    -$("#latestbox1").mouseleave(function () {
        $('.lwboxbottom1').show();
        $('#latestbox1').removeClass('hide2');
        $('.boxbuttons1').css('visibility', 'hidden');
    });
    $("#latestbox2").mouseenter(function () {
        $('.lwboxbottom2').hide();
        $('.boxbuttons2').css('visibility', 'visible');
        $('#latestbox2').addClass('hide2');
    });
    $("#latestbox2").mouseleave(function () {
        $('.lwboxbottom2').show();
        $('#latestbox2').removeClass('hide2');
        $('.boxbuttons2').css('visibility', 'hidden');
    });
    $("#latestbox3").mouseenter(function () {
        $('.lwboxbottom3').hide();
        $('.boxbuttons3').css('visibility', 'visible');
        $('#latestbox3').addClass('hide2');
    });
    $("#latestbox3").mouseleave(function () {
        $('.lwboxbottom3').show();
        $('#latestbox3').removeClass('hide2');
        $('.boxbuttons3').css('visibility', 'hidden');
    });
</script>