<section>
        <div class="row" id="headbarblack">
            <div class="col-xs-3"></div>
            <div class="col-xs-2" id="searchbardiv">
                <input type="text" placeholder="Search" size="25" class="headsearchbar">
                <button type="button" class="searchbutton"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </div>
            <div class="col-xs-7">
                <ul style="margin-right: -2%;">
                    <li class="blackheadlist"><a href="#"><span class="glyphicon glyphicon-pencil" aria-hidden="true"> Register</span></a>
                    </li>
                    <li class="blackheadlist"><a href="#"><span class="glyphicon glyphicon-user" aria-hidden="true"> Login</span></a>
                    </li>

                </ul>
            </div>
        </div>
        <div class="row" id='headergray'>
            <div class="col-xs-3"></div>
            <div class="col-xs-2"><img src="assets/images/amber.png" alt="amberimage"></div>
            <div class="col-xs-7">
                <ul>
                    <a href="#" class="headitems">
                        <li>
                            <hr id="headitem1" class="headitembar">
                            <p>HOME</p>
                        </li>
                    </a>
                    <a href="#" class="headitems">
                        <li>
                            <hr id="headitem2" class="headitembar">
                            <p>TYPOGRAPHY</p>
                        </li>
                    </a>
                    <a href="#" class="headitems">
                        <li>
                            <hr id="headitem3" class="headitembar">
                            <p>BLOG</p>
                        </li>
                    </a>
                    <a href="#" class="headitems">
                        <li>
                            <hr id="headitem4" class="headitembar">
                            <p>PAGES</p>
                        </li>
                    </a>
                    <a href="#" class="headitems">
                        <li>
                            <hr id="headitem5" class="headitembar">
                            <p>ELEMENTS</p>
                        </li>
                    </a>
                    <a href="#" class="headitems">
                        <li>
                            <hr id="headitem6" class="headitembar">
                            <p>ECOMERCE</p>
                        </li>
                    </a>
                    <a href="#" class="headitems">
                        <li>
                            <hr id="headitem7" class="headitembar">
                            <p>MEGAMENU</p>
                        </li>
                    </a>
                </ul>
            </div>
        </div>

</section>

